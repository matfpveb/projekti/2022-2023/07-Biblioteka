# Project Library

- [Overview](#overview)
  * [Basic functionalities](#basic-functionalities)
  * [Additional functionalities](#additional-functionalities)
- [Programming languages ​​and technologies](#programming-languages-​​and-technologies)
- [Instructions](#instructions)
- [Demo](#demo)
- [Documentation](#documentation)
  * [Default parameters](#default-parameters)
  * [Database schema](#database-schema)
- [Developers](#developers)


## Overview

Project Library.

### Basic functionalities

- login/sign up
- user
* view top 3 books by intakes (registered user)
* view book of the day (non-registered user)
* book search according to various criteria
* leave a comment on the book
* add book as a favorite
* reserve book
* view user profile
* change password

### Additional functionalities

- librarian
* add new book
* update book
* list registered users
* list registered books by user
* delete users

## Programming languages ​​and technologies

This project is implemented using *Node.js* for backend and *Angular* for frontend. For the database, *MongoDB* was used. Some of the additional libraries and packages used for the project are:
- *[bcrypt](https://www.npmjs.com/package/bcrypt)*
- *[jsonwebtoken](https://www.npmjs.com/package/jsonwebtoken)*
- *[express](https://www.npmjs.com/package/express)*
- *[mongoose](https://mongoosejs.com/)*
- *[muler](https://www.npmjs.com/package/multer)*

## Instructions 

1\. Clone the repository

```
git clone https://gitlab.com/matfpveb/projekti/2022-2023/07-Biblioteka.git
```

2\. Run

Run *server* and *frontend* in two terminals.

2.1. Server

- Position to *07-Biblioteka\library-server*
- run the command **npm install**
- run the command **npm start**

2.2. Frontend

- Position to *07-Biblioteka\library-frontend/*
- run the command **npm install**
- run the command **ng serve**

3\. Usage

Open in the browser *http://localhost:4200/*.


## Demo

- Demo link: [Library](https://www.youtube.com/watch?v=K6PcBOg7cVA) <br>

## Documentation

### Default parameters

Server:
- *Url* - http://localhost:4000/

Frontend:
- *Url* - http://localhost:4200/

### Database schema

<table>
<tr>
<th>Book</th>
<th>User</th>
</tr>
<tr>
<td>


| Name       | Type |
|------------|------|
| _id| mongoose.Schema.Types.ObjectId |
| thumbnailUrl| String |
| authors|  [String] |
| publishedDate| Date |
| shortDescription| String |
| genre| [String] |
| publisher| String |
| language| String |
| numIntakes| Number |
| numAvailable| Number |
| comments| [[String, String, String]] |


</td>
<td>

| Name       | Type |
|------------|------|
| _id| mongoose.Schema.Types.ObjectId |
| username| String |
| email|  String |
| favorites| [Book] |
| reserved| [Book]|
| hash| String |
| salt| String |
| librarian| Boolean |
| imgUrl| String|

</td>
</tr>
</table>

## Developers

- [Katarina Milosevic, 53/2019](https://gitlab.com/KatMilosevic)
- [Nikolina Lazarevic, 49/2019](https://gitlab.com/nickla)
- [Dragana Zdravkovic, 309/2019](https://gitlab.com/draganaZdravkovic)
- [Dimitrije Vranic, 65/2019](https://gitlab.com/Diconi)
- [Andjela Djurovic, 26/2019](https://gitlab.com/andjelaa11)
