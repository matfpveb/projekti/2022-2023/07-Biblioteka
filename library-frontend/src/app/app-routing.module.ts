import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePageComponent } from './components/home-page/home-page.component';
import { BookProfileComponent } from './components/book-profile/book-profile.component';
import { SearchBooksComponent } from './components/search-books/search-books.component';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';
import { SignupPageComponent } from './components/signup-page/signup-page.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserAuthGuard } from './guards/user-auth.guard';
import { AddBookComponent } from './components/add-book/add-book.component';

const routes: Routes = [
  {path: '', component: HomePageComponent},
  {path: 'home-page', component: HomePageComponent},
  {path: 'book-profile/:id', component: BookProfileComponent},
  {path: 'search-books', component: SearchBooksComponent},
  {path: 'profile-page', component: ProfilePageComponent},
  {path: 'login-page', component:LoginPageComponent},
  {path: 'signup-page', component:SignupPageComponent},
  //moze vise guardova u nizu
  {path: 'user-list', component: UserListComponent, canActivate: [UserAuthGuard]},
  {path: 'add-book', component: AddBookComponent, canActivate: [UserAuthGuard]}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
