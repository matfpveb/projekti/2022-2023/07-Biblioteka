import { Component, OnInit } from '@angular/core';
import { User } from './models/users.model';
import { Subscription } from 'rxjs';
import { AuthService } from './services/auth.service';

declare const $: any;

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'library-frontend';

  isLibrarian: boolean = false;
  sub: Subscription = new Subscription();
  user: User | null = null;

  constructor(private auth: AuthService) {
    this.sub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
      if (this.user?.librarian) {
        this.isLibrarian = true;
      }
    });
    this.auth.sendUserDataIfExists();
  }

  ngOnInit() {
    $('.menu .item').tab();
  }

  logout() {
    this.auth.logout();
    this.isLibrarian = false;
  }

  scroll() {
    document
      .getElementById('mainContent')
      ?.scrollIntoView({ behavior: 'smooth' });
  }
}
