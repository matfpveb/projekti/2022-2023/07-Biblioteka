import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { SearchBooksComponent } from './components/search-books/search-books.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BookProfileComponent } from './components/book-profile/book-profile.component';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';
import { LoginPageComponent } from './components/login-page/login-page.component';

import { BookService } from './services/book.service';
import { SignupPageComponent } from './components/signup-page/signup-page.component';
import { AddBookComponent } from './components/add-book/add-book.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UpdateBookComponent } from './components/update-book/update-book.component';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    SearchBooksComponent,
    BookProfileComponent,
    ProfilePageComponent,
    LoginPageComponent,
    SignupPageComponent,
    AddBookComponent,
    UserListComponent,
    UpdateBookComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule, 
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule, 
    MatFormFieldModule,
    MatInputModule,
    BrowserAnimationsModule
  ],
  providers: [BookService],
  bootstrap: [AppComponent]
})
export class AppModule { }
