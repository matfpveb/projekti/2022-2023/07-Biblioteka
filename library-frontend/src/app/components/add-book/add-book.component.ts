import { DatePipe } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/book.model';
import { BookService } from 'src/app/services/book.service';
import { TitleValidator } from 'src/app/validators/title.validator';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css'],
})
export class AddBookComponent implements OnInit {
  addBookForm: FormGroup;
  genres: Array<string> = [
    'Fantasy',
    'Sci-Fi',
    'Mystery',
    'Thriller',
    'Religon',
    'Children',
    'Adventure',
    'Musical',
    'Horror',
    'War',
    'Drama',
    'Animated',
    'Comedy',
    'Action',
    'Documentary',
    'Crime',
    'Sport',
    "Tragedy",
    'Romance',
    "Classics",
    "Fiction",
    "Philosophical novel",
    'Westerns',
    'Dystopian',
    'Contemporary',
  ].sort();
  pickedImage: string;

  constructor(
    private bookService: BookService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.addBookForm = this.formBuilder.group({
      title: [
        '',
        [Validators.required, Validators.minLength(2), TitleValidator],
      ],
      authors: [
        '',
        [
          Validators.required,
          Validators.pattern(new RegExp('^[A-Za-z-]+(?:, ?[A-Za-z-]+)*$')),
        ],
      ],
      publishedDate: ['', [Validators.required]],
      genre: [this.genres[0]],
      shortDescription: '',
      language: ['', []],
      publisher: [
        '',
        [
          Validators.required,
          Validators.minLength(2),
          Validators.pattern(new RegExp('^[A-Za-z-]+$')),
        ],
      ],
      numAvailable: [
        '',
        [Validators.required, Validators.pattern(new RegExp('^[0-9]{1,3}$'))],
      ],
      image: ['', [Validators.required]],
    });
  }

  ngOnInit() {}

  pickImage(event: Event) {
    if (event.target) {
      const file = (event.target as HTMLInputElement).files![0];
      this.addBookForm.patchValue({ image: file });
      this.addBookForm.get('image')!.updateValueAndValidity();
      const reader = new FileReader();
      reader.onload = () => {
        this.pickedImage = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  titleHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.addBookForm.get('title')?.errors;
    return errors !== null;
  }

  authorsHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.addBookForm.get('authors')?.errors;
    return errors !== null;
  }

  dateHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.addBookForm.get('date')?.errors;
    return errors !== null;
  }

  publisherHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.addBookForm.get('publisher')?.errors;
    return errors !== null;
  }

  numAvailableHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.addBookForm.get('numAvailable')?.errors;
    return errors !== null;
  }

  imageHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.addBookForm.get('image')?.errors;
    return errors !== null;
  }

  titleErrors(): string[] {
    const errors: ValidationErrors | null | undefined =
      this.addBookForm.get('title')?.errors;
    if (errors == null) {
      return [];
    }
    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('Title is required.');
    }

    if (errors['minlength']) {
      errorMsgs.push(
        `Title is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`
      );
    }

    if (errors['title']) {
      errorMsgs.push(errors['title'].message);
    }

    return errorMsgs;
  }

  dateErrors(): string[] {
    const errors: ValidationErrors | null | undefined =
      this.addBookForm.get('date')?.errors;
    if (errors == null) {
      return [];
    }
    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('Date is required.');
    }

    if (errors['date']) {
      errorMsgs.push(errors['date'].message);
    }

    return errorMsgs;
  }

  public onSubmitForm(data: any) {
    if (!this.addBookForm.valid) {
      window.alert('Form is not valid!');
      return;
    }

    let date = new DatePipe('en-US').transform(
      data.publishedDate,
      'yyyy-MM-dd'
    );

    const body = {
      title: data.title,
      thumbnailUrl: this.pickedImage,
      authors: data.authors,
      publishedDate: date,
      genre: data.genre,
      publisher: data.publisher,
      shortDescription: data.shortDescription,
      language: data.language,
      numIntakes: 0,
      numAvailable: data.numAvailable,
    };
    console.log(body);
    console.log(this.pickedImage);
    console.log(data.image);

    this.bookService.addNewBook(body).subscribe((book: Book) => {
      console.log(book);
      window.alert(`Adding book (number: ${book._id}) is successfully!`);
      this.router.navigateByUrl('/search-books');
    });

    this.addBookForm.reset({
      title: '',
      authors: '',
      publishedDate: '',
      genre: this.genres[0],
      shortDescription: '',
      language: '',
      publisher: '',
      numAvailable: '',
      image: '',
    });
  }
}
