import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/models/book.model';
import { User } from 'src/app/models/users.model';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup } from '@angular/forms';

declare const $: any;

@Component({
  selector: 'app-book-profile',
  templateUrl: './book-profile.component.html',
  styleUrls: ['./book-profile.component.css'],
})
export class BookProfileComponent implements OnInit {
  book!: Book;
  id: string;

  sub: Subscription = new Subscription();
  user: User | null = null;

  txt: String;
  date: String = new Date().toLocaleString();
  addCommentForm: FormGroup;
  comments: [[string, string, string]];

  isReserved: boolean = false;
  isFavorite: boolean = false;
  isLibrarian: boolean = false;

  constructor(
    private activatedRoute: ActivatedRoute,
    private bookService: BookService,
    private auth: AuthService,
    private usersService: UsersService,
    private formBuilder: FormBuilder
  ) {
    this.sub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();

    // get current book
    this.id = this.activatedRoute.snapshot.paramMap.get('id')!;
    this.bookService.getBookById(this.id).subscribe((book) => {
      this.book = book as Book;
      this.getIsReserved();
      this.getIsFavorite();
    });

    this.addCommentForm = this.formBuilder.group({
      comments: '',
    });

    this.isLibrarian = this.user?.librarian!;
  }

  ngOnInit() {
    this.GetComments();
    this.bookService.RefreshCommentsRequired.subscribe((response) => {
      this.GetComments();
    });
  }

  GetComments() {
    this.bookService.getComments(this.id).subscribe((result) => {
      this.comments = result;
    });
  }

  ReserveBook() {
    this.isReserved = true;
    this.bookService
      .reserveBook(this.id, this.user?.username)
      .subscribe((result) => {});
  }

  RemoveReservedBook() {
    this.isReserved = false;
    this.bookService
      .removeFromReserved(this.id, this.user?.username)
      .subscribe((result) => {});
  }

  FavoriteBook() {
    this.isFavorite = true;
    this.usersService
      .addToFavorite(this.id, this.user?.username)
      .subscribe((result) => {});
  }

  RemoveFavoriteBook() {
    this.isFavorite = false;
    this.usersService
      .removeFavorite(this.id, this.user?.username)
      .subscribe((result) => {});
  }

  getIsReserved() {
    var reservedId: string[] = [];
    this.bookService
      .getReserved(this.user!.username)
      .subscribe((fav: string[]) => {
        reservedId = fav;

        this.isReserved = reservedId.includes(this.book._id);
      });
  }

  getIsFavorite() {
    this.usersService
      .getFavorites(this.user?.username)
      .subscribe((fav: string[]) => {
        this.isFavorite = fav.includes(this.book._id);
      });
  }

  onAddText(event: Event): void {
    const txt = (<HTMLInputElement>event.target).value;
    this.txt = txt;
  }

  submitForm(data: any) {
    this.addCommentForm.reset({
      comment: '',
    });
    this.bookService
      .addComment(this.book._id, this.user?.username, this.txt, this.date)
      .subscribe((book: Book) => {});
  }
}
