import { Component, OnInit } from '@angular/core';
import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/models/book.model';
import { Subscription } from 'rxjs';
import { User } from 'src/app/models/users.model';
import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css'],
})
export class HomePageComponent implements OnInit {
  topBooks: Book[] = [];
  topBook: Book = {} as Book;

  sub: Subscription = new Subscription();
  user: User | null = null;

  constructor(private bookService: BookService, private auth: AuthService) {
    this.sub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();

    this.bookService.getBooks().subscribe((books) => {
      this.topBooks = books as Book[];
      console.log(books);
    });

    this.bookService.getBooks().subscribe((books) => {
      const result = books as Book[];
      const randIndex = Math.floor(Math.random() * result.length);
      this.topBook = result[randIndex];
    });
  }

  ngOnInit() {
    if (this.user === null) {
      // return all books (must have at least three books in database)
      this.bookService.getBooks().subscribe((books) => {
        this.topBooks = books as Book[];
        console.log(books);
      });
    } else {
      // return random book
      this.bookService.getBooks().subscribe((books) => {
        const result = books as Book[];
        const randIndex = Math.floor(Math.random() * result.length);
        this.topBook = result[randIndex];
      });
    }
  }
}
