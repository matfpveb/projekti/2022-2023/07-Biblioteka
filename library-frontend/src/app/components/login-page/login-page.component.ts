import { Component, OnDestroy, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
  ValidationErrors,
} from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/users.model';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';
declare const $: any;

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css'],
})
export class LoginPageComponent implements OnInit, OnDestroy {
  sub: Subscription = new Subscription();
  user: User;
  loginForm: FormGroup;
  changeUserField: string = 'field';

  constructor(private auth: AuthService, private router: Router) {
    this.loginForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }

  login(): void {
    if (this.loginForm.invalid) {
      window.alert('Form is not valid!');
    }

    const data = this.loginForm.value;
    const obs: Observable<User | null> = this.auth.login(
      data.username,
      data.password
    );

    this.sub = obs.subscribe(
      (user: User | null) => {
        console.log(user);
        this.router.navigateByUrl('/profile-page');
      },
      (err) => {
        if (err.status !== 200) {
          window.alert('Invalid email or password. Please try again.');
        }
      }
    );

    this.auth.login(data.username, data.password);
  }

  passwordHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.loginForm.get('password')?.errors;

    return errors !== null;
  }

  passwordErrors(): string[] {
    const errors: ValidationErrors | null | undefined =
      this.loginForm.get('password')?.errors;
    if (errors == null) {
      return [];
    }
    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('You must enter a password.');
    }

    if (errors['minlength']) {
      errorMsgs.push(
        ` Password is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`
      );
    }

    if (errors['passwordText']) {
      errorMsgs.push(errors['passwordText'].message);
    }

    return errorMsgs;
  }

  nameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.loginForm.get('username')?.errors;

    return errors !== null;
  }

  nameErrors(): string[] {
    const errors: ValidationErrors | null | undefined =
      this.loginForm.get('username')?.errors;
    if (errors == null) {
      return [];
    }
    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('You username.');
    }

    if (errors['minlength']) {
      errorMsgs.push(
        ` UserName is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`
      );
    }

    if (errors['UserName']) {
      errorMsgs.push(errors['UserName'].message);
    }

    return errorMsgs;
  }
}
