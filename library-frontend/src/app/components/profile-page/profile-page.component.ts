import { Component, OnInit } from '@angular/core';
import {  Subscription } from 'rxjs';
import { User } from 'src/app/models/users.model';
import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/models/book.model';
import { UsersService } from 'src/app/services/users.service';
import { AuthService } from 'src/app/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.css'],
})
export class ProfilePageComponent implements OnInit {
  reservedBooks$: Book[];
  favoritedBooks$: Book[];

  isFavorites: boolean = true;

  sub: Subscription = new Subscription();
  user: User | null = null;

  changePassForm: FormGroup;
  FormBuilder: any;

  shouldChangePassword:boolean = false;

  constructor(
    private usersService: UsersService,
    private bookService: BookService,
    private auth: AuthService,
    private formBuilder: FormBuilder,
    private router: Router
  ) {
    this.sub = this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();
  }

  ngOnInit(): void {
    this.GetReserved();
    this.bookService.RefreshRequired.subscribe((response) => {
      this.GetReserved();
    });

    this.GetFavorites();
    this.usersService.RefreshRequired.subscribe((response) => {
      this.GetFavorites();
    });

    this.changePassForm = this.formBuilder.group({
      oldPass: ['', [Validators.required]],
      newPass: ['', [Validators.required]],
    });
  }

  GetReserved() {
    this.isFavorites = false;
    this.bookService.getReserved(this.user!.username).subscribe((result) => {
      var tmp: Book[] = [];
      result.forEach((resId) => {
        this.bookService.getBookById(resId).subscribe((book: Book) => {
          tmp.push(book);
        });
      });
      this.reservedBooks$ = tmp;
    });
  }

  RemoveReserved(bookId: string) {
    this.bookService
      .removeFromReserved(bookId, this.user!.username)
      .subscribe((result) => {});
  }

  GetFavorites() {
    this.isFavorites = true;

    this.usersService.getFavorites(this.user!.username).subscribe((result) => {
      var tmp: Book[] = [];
      result.forEach((resId) => {
        this.bookService.getBookById(resId).subscribe((book: Book) => {
          tmp.push(book);
        });
      });
      this.favoritedBooks$ = tmp;
    });
  }

  RemoveFavorite(bookId: string) {
    this.usersService
      .removeFavorite(bookId, this.user!.username)
      .subscribe((result) => {});
  }

  changePassword(){
    this.shouldChangePassword = !this.shouldChangePassword
  }

  onSubmitForm(data: any) {
    if (!this.changePassForm.valid) {
      window.alert('Form is not valid!');
      return;
    }

    const body = {
      username: this.user?.username,
      oldPassword: data.oldPass,
      newPassword: data.newPass,
    };

    this.usersService.changePassword(body).subscribe(
      (user: User) => {
        window.alert(`Password is successfully changed!`);
        this.auth.logout();
        this.router.navigateByUrl('/login-page');
      },
      (err) => {
        window.alert('Old password is incorrect!');
      }
    );

    this.changePassForm.reset({
      oldPass: '',
      newPass: '',
    });
  }
}
