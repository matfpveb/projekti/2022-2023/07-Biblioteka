import { Component, OnDestroy, OnInit } from '@angular/core';
import { Observable, Subscription } from 'rxjs';
import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/models/book.model';

@Component({
  selector: 'app-search-books',
  templateUrl: './search-books.component.html',
  styleUrls: ['./search-books.component.css'],
})
export class SearchBooksComponent implements OnInit, OnDestroy {
  public books: Book[] = [];
  public searchedBooks: Book[] = [];
  public searchParam: string;
  public searchType: string;
  private activeSubscriptions: Subscription[] = [];
  public showAll: boolean;
  public sortType: string;

  constructor(private bookService: BookService) {
    const sub = this.bookService.getBooks().subscribe((books: Book[]) => {
      this.books = books;
    });
    this.activeSubscriptions.push(sub);

    this.showAll = true;
    this.searchType = 'title';
    this.sortType = 'publishedDate';
  }

  ngOnInit() {}

  ngOnDestroy() {
    this.activeSubscriptions.forEach((sub) => sub.unsubscribe);
  }

  onSelectedType(event: Event): void {
    this.searchType = (<HTMLSelectElement>event.target).value;
  }

  onSearch() {
    let sub;

    this.showAll = false;
    if (this.searchType === 'title') {
      sub = this.bookService
        .getBookByTitle(this.searchParam)
        .subscribe((books: Book[]) => {
          this.searchedBooks = books;
        });
    } else if (this.searchType === 'author') {
      sub = this.bookService
        .getBookByAuthor(this.searchParam)
        .subscribe((books: Book[]) => {
          this.searchedBooks = books;
        });
    } else if (this.searchType === 'genre') {
      sub = this.bookService
        .getBookByGenre(this.searchParam)
        .subscribe((books: Book[]) => {
          this.searchedBooks = books;
        });
    } else if (this.searchType === 'language') {
      sub = this.bookService
        .getBookByLanguage(this.searchParam)
        .subscribe((books: Book[]) => {
          this.searchedBooks = books;
        });
    } else {
      sub = this.bookService
        .getBookByTitle(this.searchParam)
        .subscribe((books: Book[]) => {
          this.searchedBooks = books;
        });
    }

    this.activeSubscriptions.push(sub);
  }

  onSort(event: Event): void {
    this.sortType = (<HTMLSelectElement>event.target).value;
    let sub;

    if (this.sortType == 'title') {
      sub = this.bookService.sortByTitle().subscribe((books: Book[]) => {
        this.books = books;
      });
    } else if (this.sortType == 'publishedDate') {
      sub = this.bookService.sortByDate().subscribe((books: Book[]) => {
        this.books = books;
      });
    } else {
      sub = this.bookService.sortByTitle().subscribe((books: Book[]) => {
        this.books = books;
      });
    }

    this.activeSubscriptions.push(sub);
  }
}
