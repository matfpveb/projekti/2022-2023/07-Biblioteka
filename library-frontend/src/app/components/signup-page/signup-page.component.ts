import { Component, OnInit } from '@angular/core';
import { UserNameValidator } from 'src/app/validators/user-name.validator';
import { PasswordValidator } from 'src/app/validators/password.validator';
import { EmailValidator } from 'src/app/validators/email.validator';
import {
  FormControl,
  FormGroup,
  Validators,
  ValidationErrors,
} from '@angular/forms';
import { Observable, Subscription } from 'rxjs';
import { User } from 'src/app/models/users.model';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-signup-page',
  templateUrl: './signup-page.component.html',
  styleUrls: ['./signup-page.component.css'],
})
export class SignupPageComponent implements OnInit {
  user: User;
  prevUser: User | null;
  sub: Subscription;
  createUserForm: FormGroup;
  pickedImage: string = "assets/blank-profile.png";

  constructor(private auth: AuthService, private router: Router) {
    this.createUserForm = new FormGroup({
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(4),
        UserNameValidator,
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        PasswordValidator,
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email,
        Validators.minLength(5),
        EmailValidator,
      ]),
      image: new FormControl('',[])
    });
  }

  ngOnInit(): void {}

  ngOnDestroy(): void {
    this.sub?.unsubscribe();
  }

  pickImage(event: Event){
    if (event.target){
      const file = (event.target as HTMLInputElement).files![0];
      this.createUserForm.patchValue({image: file});
      this.createUserForm.get('image')!.updateValueAndValidity();
      const reader = new FileReader();
      reader.onload = ()=>{  
        this.pickedImage = reader.result as string;
      };
      reader.readAsDataURL(file);
    }
  }

  register() {
    if (this.createUserForm.invalid) {
      window.alert('From is not valid!');
      return;
    }

    const data = this.createUserForm.value;
    const username = data.username;
    const password = data.password;
    const email = data.email;
    const imgUrl = this.pickedImage;

    const obs: Observable<User | null> = this.auth.registerUser(
      username,
      password,
      email,
      imgUrl
    );

    this.sub = obs.subscribe(
      (user: User | null) => {
        console.log(user);
        this.router.navigateByUrl('/login-page');
      },
      (err) => {
        if (err.status !== 200) {
          window.alert(
            'Username already exists! Please try again with another username.'
          );
        }
      }
    );
  }
  emailHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.createUserForm.get('email')?.errors;

    return errors !== null;
  }

  emailErrors(): string[] {
    const errors: ValidationErrors | null | undefined =
      this.createUserForm.get('email')?.errors;
    if (errors == null) {
      return [];
    }
    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('You must enter a password.');
    }

    if (errors['minlength']) {
      errorMsgs.push(
        ` Email is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`
      );
    }

    if (errors['emailText']) {
      errorMsgs.push(errors['emailText'].message);
    }

    return errorMsgs;
  }

  passwordHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.createUserForm.get('password')?.errors;

    return errors !== null;
  }

  passwordErrors(): string[] {
    const errors: ValidationErrors | null | undefined =
      this.createUserForm.get('password')?.errors;
    if (errors == null) {
      return [];
    }
    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('You must enter a password.');
    }

    if (errors['minlength']) {
      errorMsgs.push(
        ` Password is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`
      );
    }

    if (errors['passwordText']) {
      errorMsgs.push(errors['passwordText'].message);
    }

    return errorMsgs;
  }

  nameHasErrors(): boolean {
    const errors: ValidationErrors | null | undefined =
      this.createUserForm.get('username')?.errors;

    return errors !== null;
  }

  nameErrors(): string[] {
    const errors: ValidationErrors | null | undefined =
      this.createUserForm.get('username')?.errors;
    if (errors == null) {
      return [];
    }
    const errorMsgs: string[] = [];

    if (errors['required']) {
      errorMsgs.push('User must have a username.');
    }

    if (errors['minlength']) {
      errorMsgs.push(
        ` UserName is ${errors['minlength'].actualLength} characters long, when it should be at least ${errors['minlength'].requiredLength} characters.`
      );
    }

    if (errors['UserName']) {
      errorMsgs.push(errors['UserName'].message);
    }

    return errorMsgs;
  }
}