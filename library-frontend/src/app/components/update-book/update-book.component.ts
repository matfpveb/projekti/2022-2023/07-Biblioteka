import { Component,Input} from '@angular/core';
import { FormGroup , FormBuilder} from '@angular/forms';
import { BookService } from 'src/app/services/book.service';
import { Book } from 'src/app/models/book.model';
import { Router } from '@angular/router';

@Component({
  selector: 'app-update-book',
  templateUrl: './update-book.component.html',
  styleUrls: ['./update-book.component.css']
})
export class UpdateBookComponent {

  updateBookForm: FormGroup;

  @Input('book!') book : Book;

  constructor(private bookService: BookService, 
    private formBuilder: FormBuilder, private router: Router){
      this.updateBookForm = this.formBuilder.group({
        title: ['', ],
        authors: ['', ],
        genre: [],
        language: '',
        publisher: ['', ],
      })
    }

    ngOnInit(){
      this.GetBook();
    this.bookService.RefreshRequired.subscribe((response) => {
      this.GetBook();
    })
    }

    GetBook() {
      this.bookService.getBookById(this.book._id).subscribe((result) => {
        this.book = result
      });
    }

    public onSubmitForm(data: any){
      if(!this.updateBookForm.valid){
        window.alert('Form is not valid!');
        return;
      }  
  
      const body = {
        id: this.book._id,
        title: data.title,
        authors: data.authors,
        genre: data.genre,
        publisher: data.publisher,
        language: data.language,
      }
      console.log(body);
  
      this.bookService.updateBook(body)
          .subscribe((book: Book) => {
            window.alert(
              `Updating book is successful!`);  
          })
        
      this.updateBookForm.reset({
        title: '',
        authors: '',
        genre: '',
        language: '',
        publisher: ''
  
      })
    }

    onDeleteBook() {
      this.bookService.deleteBook(this.book._id).subscribe((book: Book) => {
        console.log(book)
        window.alert(
          `Deleting book is successful!`);   
        this.router.navigateByUrl('/search-books');        
      });
    }
}


