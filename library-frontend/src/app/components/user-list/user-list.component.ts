import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/users.model';
import { AuthService } from 'src/app/services/auth.service';
import { UsersService } from 'src/app/services/users.service';
import { BookService } from 'src/app/services/book.service';

declare const $: any;
@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css'],
})
export class UserListComponent implements OnInit {
  userList: User[];
  titleList: string[] = [];
  calledUser: string = '';

  user: User | null = null;

  constructor(
    private userService: UsersService,
    private auth: AuthService,
    private bookService: BookService
  ) {
    this.auth.user.subscribe((user: User | null) => {
      this.user = user;
    });
    this.auth.sendUserDataIfExists();
  }

  ngOnInit() {
    this.GetUserList();
    this.userService.RefreshUsersListRequired.subscribe((response) => {
      this.GetUserList();
    });
  }

  GetUserList() {
    this.userService.getUsers().subscribe((result) => {
      this.userList = result;
    });
  }

  DeleteUser(username: string) {
    this.userService.deleteUser(username).subscribe((user) => {});
  }

  GetReserved(username: string) {
    this.titleList = [];
    this.calledUser = username;
    this.bookService.getReservedTitles(username).subscribe((result) => {
      this.titleList = result;
    });
  }
}
