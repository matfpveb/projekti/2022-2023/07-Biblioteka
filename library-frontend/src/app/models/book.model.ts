export interface Book {
  _id: string;
  title: string;
  thumbnailUrl: string;
  authors: string[];
  publishedDate: Date;
  shortDescription: string;
  genre: string[];
  publisher: string;
  language: string;
  numIntakes: number;
  numAvailable: number;
  comments: [[string, string, string]];
}
