export interface IJwtTokenData {
    _id: string,
    username: string,
    email: string,
    imgUrl: string,
    favorites: string[],
    reserved: string[],
    librarian: boolean
}
