export class User {
  constructor(
    public _id: string,
    public username: string,
    public email: string,
    public imgUrl:string="assets/blank-profile.png",
    public favorites: string[] = [],
    public reserved: string[] = [],
    public librarian: boolean
  ) {}
}
