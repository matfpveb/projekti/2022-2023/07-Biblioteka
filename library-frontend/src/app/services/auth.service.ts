import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable, Subject, tap } from 'rxjs';
import { User } from '../models/users.model';
import { JwtService } from './jwt.service';
import { IJwtTokenData } from '../models/jwt-token-data';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly urls = {
    signupUrl: 'http://localhost:4000/users/signup',
    loginUrl: 'http://localhost:4000/users/login',
  };

  public isLoggedIn: boolean = false;
  public isLibrarian: boolean = false;

  public readonly userSubject: Subject<User | null> = new Subject();
  public readonly user: Observable<User | null> =
    this.userSubject.asObservable();

  constructor(private http: HttpClient, private jwt: JwtService) {}


  public registerUser(
    username: string,
    password: string,
    email: string,
    imgUrl: string
  ): Observable<User | null> {
    const body = {
      username,
      password,
      email,
      imgUrl
    };

    const obs: Observable<{ token: string }> = this.http.post<{
      token: string;
    }>(this.urls.signupUrl, body);

    // prvo treba sacuvati token, a onda pretvoriti u korisnika
    return obs.pipe(
      tap((response: { token: string }) => this.jwt.setToken(response.token)),
      map((response: { token: string }) => this.sendUserDataIfExists())
    );
  }

  public login(username: string, password: string): Observable<User | null> {
    const body = {
      username,
      password,
    };
    const obs: Observable<{ token: string }> = this.http.post<{
      token: string;
    }>(this.urls.loginUrl, body);

    // prvo treba sacuvati token, a onda ga 'pretvoriti' u korisnika
    return obs.pipe(
      tap((response: { token: string }) => this.jwt.setToken(response.token)),
      map((response: { token: string }) => this.sendUserDataIfExists())
    );
  }

  public sendUserDataIfExists(): User | null {
    const payload: IJwtTokenData | null = this.jwt.getDataFromToken();
    if (!payload) return null;

    const newUser = new User(
      payload._id,
      payload.username,
      payload.email,
      payload.imgUrl,
      payload.favorites,
      payload.reserved,
      payload.librarian
    );
    this.userSubject.next(newUser);
    this.isLoggedIn = true;
    this.isLibrarian = false;
    if (newUser.librarian) {
      this.isLibrarian = true;
    }
    return newUser;
  }

  public logout(): void {
    // Uklanjamo JWT iz memorije i saljemo svim komponentama da korisnik "ne postoji"
    this.jwt.removeToken();
    this.userSubject.next(null);
    this.isLoggedIn = false;
  }
}
