import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Book } from '../models/book.model';
import { identity, Observable, Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  private books: Observable<Book[]> = new Observable<Book[]>();

  private readonly booksUrl = 'http://localhost:4000/books/';

  private refreshRequired = new Subject<void>();
  private refreshCommentsRequired = new Subject<void>();

  get RefreshRequired() {
    return this.refreshRequired;
  }

  get RefreshCommentsRequired() {
    return this.refreshCommentsRequired;
  }

  constructor(private http: HttpClient) {
    this.refreshBooks();
  }

  private refreshBooks(): Observable<Book[]> {
    this.books = this.http.get<Book[]>(this.booksUrl);
    return this.books;
  }

  public sortByTitle(): Observable<Book[]> {
    return this.http.get<Book[]>(this.booksUrl + 'sortByTitle');
  }

  public sortByDate(): Observable<Book[]> {
    return this.http.get<Book[]>(this.booksUrl + 'sortByDate');
  }

  getBookById(id: string): Observable<Book> {
    return this.http.get<Book>(this.booksUrl + id);
  }

  public deleteBook(id: string): Observable<Book> {
    const params: HttpParams = new HttpParams().append('id', id);

    return this.http.delete<Book>(this.booksUrl, { params: params });
  }

  public getBooks(): Observable<Book[]> {
    return this.books;
  }

  getBookByTitle(title: string) {
    const params: HttpParams = new HttpParams().append('title', title);

    return this.http.get<Book[]>(this.booksUrl + 'title', { params: params });
  }

  getBookByAuthor(authors: string) {
    const data = {
      authors: authors,
    };

    return this.http.post<Book[]>(this.booksUrl + 'author', data);
  }

  getBookByGenre(genre: string) {
    const params: HttpParams = new HttpParams().append('genre', genre);

    return this.http.get<Book[]>(this.booksUrl + 'genre', { params: params });
  }

  getBookByLanguage(language: string) {
    const params: HttpParams = new HttpParams().append('language', language);

    return this.http.get<Book[]>(this.booksUrl + 'language', {
      params: params,
    });
  }

  getBooksByRating(rating: string) {
    const params: HttpParams = new HttpParams().append('rating', rating);

    return this.http.get<Book[]>(this.booksUrl + 'rating', { params: params });
  }

  addNewBook(data: any) {
    return this.http.post<Book>(this.booksUrl, data);
  }

  reserveBook(id: String, username: String | undefined): Observable<Book> {
    const data = { id, username };

    return this.http.put<Book>(this.booksUrl + 'reserve', data).pipe(
      tap(() => {
        this.refreshRequired.next();
      })
    );
  }

  public removeFromReserved(
    id: String,
    username: String | undefined
  ): Observable<Book> {
    const data = { id, username };

    return this.http.put<Book>(this.booksUrl + 'removeReserved', data).pipe(
      tap(() => {
        this.refreshRequired.next();
      })
    );
  }

  public getReserved(username: string): Observable<string[]> {
    return this.http.get<string[]>(this.booksUrl + '/reserved/' + username);
  }

  public getReservedTitles(username: string): Observable<string[]> {
    return this.http.get<string[]>(
      this.booksUrl + '/reservedTitles/' + username
    );
  }

  public addComment(
    id: String,
    username: String | undefined,
    txt: String,
    date: String
  ): Observable<Book> {
    const data = { id, username, txt, date };

    return this.http.put<Book>(this.booksUrl + 'addComment', data);
  }

  public getComments(bookId: String) {
    const data = {
      id: bookId,
    };

    return this.http
      .post<[[string, string, string]]>(this.booksUrl + 'comments', data)
      .pipe(
        tap(() => {
          this.refreshCommentsRequired.next();
        })
      );
  }

  public updateBook(data: any): Observable<Book> {
    return this.http.put<Book>(this.booksUrl + 'updateBook', data).pipe(
      tap(() => {
        this.refreshRequired.next();
      })
    );
  }
}
