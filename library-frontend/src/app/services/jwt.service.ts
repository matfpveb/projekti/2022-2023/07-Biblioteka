import { Injectable } from '@angular/core';
import { IJwtTokenData } from '../models/jwt-token-data';

//parsiranje jwt tokena

@Injectable({
  providedIn: 'root',
})
export class JwtService {
  private static readonly USER_TOKEN_ID = 'USER_JWT_TOKEN';

  constructor() {}

  public setToken(jwt: string): void {
    localStorage.setItem(JwtService.USER_TOKEN_ID, jwt);
  }

  public getToken(): string {
    const token: string | null = localStorage.getItem(JwtService.USER_TOKEN_ID);

    if (!token) {
      return '';
    }

    return token;
  }

  //zbog logout
  public removeToken() {
    localStorage.removeItem(JwtService.USER_TOKEN_ID);
  }

  public getDataFromToken(): IJwtTokenData | null {
    const token = this.getToken();

    if (!token) {
      return null;
    }

    const payloadString: string = token.split('.')[1]; //json
    const userDataJson: string = atob(payloadString); //desifrovannje

    const payload: IJwtTokenData = JSON.parse(userDataJson);

    return payload;
  }
}
