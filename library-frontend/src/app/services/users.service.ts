import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { User } from '../models/users.model';
import { Observable, Subject, tap } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UsersService {
  users: Observable<User[]> = new Observable<User[]>();

  private readonly usersUrl = 'http://localhost:4000/users/';

  private refreshRequired = new Subject<void>();

  private refreshUsersListRequired = new Subject<void>();

  get RefreshRequired() {
    return this.refreshRequired;
  }

  get RefreshUsersListRequired() {
    return this.refreshUsersListRequired;
  }

  constructor(private http: HttpClient) {
    this.refreshUsers();
  }

  private refreshUsers(): Observable<User[]> {
    this.users = this.http.get<User[]>(this.usersUrl);
    return this.users;
  }

  public getUsers(): Observable<User[]> {
    return this.http.get<User[]>(this.usersUrl).pipe(
      tap(() => {
        this.refreshRequired.next();
      })
    );
  }

  public deleteUser(username: string): Observable<User> {
    return this.http.delete<User>(this.usersUrl + username).pipe(
      tap(() => {
        this.refreshUsersListRequired.next();
      })
    );
  }

  public changePassword(data: any) {
    return this.http.put<User>(this.usersUrl, data);
  }

  public getUserByUsername(username: string): Observable<User> {
    return this.http.get<User>(this.usersUrl + username);
  }

  addToFavorite(id: String, username: String | undefined): Observable<User> {
    const data = { id, username };

    return this.http.put<User>(this.usersUrl + 'addFavorite', data).pipe(
      tap(() => {
        this.refreshRequired.next();
      })
    );
  }

  public removeFavorite(
    id: String,
    username: String | undefined
  ): Observable<User> {
    const data = { id, username };

    return this.http.put<User>(this.usersUrl + 'removeFavorite', data).pipe(
      tap(() => {
        this.refreshRequired.next();
      })
    );
  }

  public getFavorites(username: string | undefined): Observable<string[]> {
    return this.http.get<string[]>(this.usersUrl + '/favorite/' + username);
  }
}
