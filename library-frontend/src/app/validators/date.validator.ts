import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const DateValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const dateParts: string[] = control.value.trim().split('/');

    if (Number(dateParts) != 3){
        return {
            date: {
                message: "Date must have day, month and year."
            }
        } 
    }

    if(!dateParts[0].match(new RegExp("0[1-9]|[12][0-9]|3[01]"))){      
        return {
            date: {
                message: "Day has irregular format."
            }
        } 
    }

    if(!dateParts[1].match(new RegExp("0[1-9]|1[012]"))){      
        return {
            date: {
                message: "Month has irregular format."
            }
        } 
    }

    if(!dateParts[2].match(new RegExp("19\d{2}|20[01][0-9]|202[0123]"))){      
        return {
            date: {
                message: "Year has irregular format."
            }
        } 
    }

    return null;

}
