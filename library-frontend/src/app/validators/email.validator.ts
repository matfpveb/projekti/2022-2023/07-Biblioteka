import { getNumberOfCurrencyDigits } from "@angular/common";
import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const EmailValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const nameParts: string[] = control.value.trim().split(' ')
        .filter((namePart: string) => namePart.trim().length > 0);

    if (nameParts.length === 0) {
        return {
            emailText: {
                message: "Email  cannot consist of whitespaces only!"
            }
        }
    }


    if(  nameParts.every((namePart:string) => namePart.match(new RegExp("^[a-zA-Z]+$")) ) )      {
        
        return {
            emailText: {
                message: "Email must contain  @ sign"
            }
        } 


    }
    if(  nameParts.every((namePart:string) => namePart.match(new RegExp("^[a-zA-Z]+@[a-zA-Z]*$")) ) )      {
        
        return {
            emailText: {
                message: "Email must contain  .com"
            }
        } 


    }
    if(  nameParts.every((namePart:string) => namePart.match(new RegExp("^[a-zA-Z]+@[a-zA-Z]*\.$")) ) )      {
        
        return {
            emailText: {
                message: "Email must contain  .com"
            }
        } 


    }
    if(  nameParts.every((namePart:string) => namePart.match(new RegExp("^[a-zA-Z]+@[a-zA-Z]*\.c$")) ) )      {
        
        return {
            emailText: {
                message: "Email must contain  .com"
            }
        } 


    }
    if(  nameParts.every((namePart:string) => namePart.match(new RegExp("^[a-zA-Z]+@[a-zA-Z]*\.co$")) ) )      {
        
        return {
            emailText: {
                message: "Email must contain  .com"
            }
        } 


    }
  
   
    return null;
}
