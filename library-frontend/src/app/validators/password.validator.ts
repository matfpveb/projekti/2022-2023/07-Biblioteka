import { getNumberOfCurrencyDigits } from "@angular/common";
import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const PasswordValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const nameParts: string[] = control.value.trim().split(' ')
        .filter((namePart: string) => namePart.trim().length > 0);

    if (nameParts.length === 0) {
        return {
            passwordText: {
                message: "Password  cannot consist of whitespaces only!"
            }
        }
    }


    if(  nameParts.every((namePart:string) => namePart.match(new RegExp("^[a-zA-Z]+$")) ) )      {
        
        return {
            passwordText: {
                message: "Password must contain at least two digit number"
            }
        } 


    }
    if (nameParts.every((namePart: string) => namePart.match(new RegExp("^[0-9 ]+$")))) {
        
        return {
            passwordText: {
                message: "Password cannot be only digits!"
            }
        }
    }
    if(  nameParts.every((namePart:string) => namePart.match(new RegExp("^[a-zA-Z]*[0-9][a-zA-Z]*$")) ) )      {
        
        return {
            passwordText: {
                message: "Password must contain at least two digit number"
            }
        } 


    }
    

    return null;
}
