import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const TitleValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const titleParts: string[] = control.value.trim().split(' ')
        .filter((titlePart: string) => titlePart.trim().length > 0);

    if (titleParts.length === 0) {
        return {
            title: {
                message: "Title cannot consist of whitespaces only!"
            }
        }
    }

    for (let titlePart of titleParts) {
        if (!titlePart.match(new RegExp(/^[A-Za-z-]+$/))) {
            return {
                title: {
                    message: "Title part can contain only characters and -"
                }
            }
        }
    }

    return null;
}
