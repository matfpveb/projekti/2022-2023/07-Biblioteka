import { AbstractControl, ValidationErrors, ValidatorFn } from "@angular/forms";

export const UserNameValidator: ValidatorFn = (control: AbstractControl): ValidationErrors | null => {
    const nameParts: string[] = control.value.trim().split(' ')
        .filter((namePart: string) => namePart.trim().length > 0);

    if (nameParts.length === 0) {
        return {
            UserName: {
                message: "User name cannot consist of whitespaces only!"
            }
        }
    }

    if (nameParts.every((namePart: string) => namePart.match(new RegExp("^[0-9 ]+$")))) {
        return {
            UserName: {
                message: "User name cannot be only digits!"
            }
        }
    }

    return null;
}
