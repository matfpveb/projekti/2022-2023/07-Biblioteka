const express = require("express");
const usersRouter = require("./routes/api/users");
const booksRouter = require("./routes/api/books");
const { urlencoded, json } = require("body-parser");
const exp = require("constants");
const mongoose = require("mongoose");
const cors = require("cors");

const app = express();
app.use(cors());

const dbString = "mongodb://127.0.0.1:27017/library";

mongoose.set("strictQuery", false);
mongoose.connect(dbString, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

mongoose.connection.once("open", function () {
  console.log("Successfull connection to db");
});

mongoose.connection.on("error", function (err) {
  console.log(err);
});

// Parsiranje tela zahteva za dva formata:
// 1) application/x-www-form-urlencoded
app.use(
  express.urlencoded({
    extended: false,
  })
);
// 2) application/json
app.use(express.json());

// Implementacija CORS zastite
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");

  if (req.method === "OPTIONS") {
    res.header(
      "Access-Control-Allow-Methods",
      "OPTIONS, GET, POST, PATCH, PUT, DELETE"
    );

    return res.status(200).json({});
  }

  next();
});

app.use("/users", usersRouter);
app.use("/books", booksRouter);
app.get("/", (req, res) => {
  res.send("Hello World!");
});

// Obrada zahteva koji se ne poklapa sa nekom pravilom od iznad
app.use(function (req, res, next) {
  const error = new Error("Zahtev nije podrzan od servera");
  error.status = 405;

  next(error);
});

// Obrada svih gresaka u nasoj aplikaciji
app.use(function (error, req, res, next) {
  // console.error(error.stack);

  const statusCode = error.status || 500;
  res.status(statusCode).json({
    message: error.message,
    status: statusCode,
    stack: error.stack,
  });
});

module.exports = app;
