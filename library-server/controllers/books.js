const bookExport = require("../models/book.js");
const userExport = require("../models/users.js");

const mongoose = require("mongoose");

const bookModel = bookExport.bookModel;
const userModel = userExport.usersModel;

const getBooks = async (req, res, next) => {
  try {
    const bookObject = await bookModel.find().sort({ numIntakes: -1 }).exec();

    if (bookObject) {
      res.status(200).json(bookObject);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const getBookById = async (req, res, next) => {
  const id = req.params.id;
  try {
    if (!id) {
      const error = new Error("Requested id is missing from the database.");
      error.status = 400;
      throw error;
    }
    const bookObject = await bookModel.findOne({ _id: id }).exec();

    if (bookObject) {
      res.status(200).json(bookObject);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const getBookByTitle = async (req, res, next) => {
  const title = req.query.title;
  try {
    if (!title) {
      const error = new Error("Requested title is missing from the database.");
      error.status = 400;
      throw error;
    }
    const reg = new RegExp(".*\\b" + title + ".*", "i");
    const bookObject = await bookModel.find({ title: { $regex: reg } }).exec();

    if (bookObject) {
      res.status(200).json(bookObject);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const getBookByAuthor = async (req, res, next) => {
  const { authors } = req.body;

  try {
    if (!authors) {
      const error = new Error("Requested author is missing from the database.");
      error.status = 400;
      throw error;
    }

    const reg = new RegExp(".*" + authors + ".*", "i");
    const knj = await bookModel.find({ authors: { $regex: reg } }).exec();

    res.status(200).json(knj);
  } catch (error) {
    next(error);
  }
};

const getBooksByGenre = async (req, res, next) => {
  const genre = req.query.genre;
  try {
    if (!genre) {
      const error = new Error("Requested genre is missing from the database.");
      error.status = 400;
      throw error;
    }
    const reg = new RegExp(".*\\b" + genre + ".*", "i");
    const booksObject = await bookModel.find({ genre: { $regex: reg } }).exec();

    if (booksObject) {
      res.status(200).json(booksObject);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const getBooksByLanguage = async (req, res, next) => {
  const language = req.query.language;
  try {
    if (!language) {
      const error = new Error(
        "Requested language is missing from the database."
      );
      error.status = 400;
      throw error;
    }
    const reg = new RegExp(".*\\b" + language + ".*", "i");
    const booksObject = await bookModel
      .find({ language: { $regex: reg } })
      .exec();

    if (booksObject) {
      res.status(200).json(booksObject);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const sortByTitle = async function (req, res, next) {
  try {
    const books = await bookModel.find({}).sort({ title: 1 }).exec();

    if (books) {
      res.status(200).json(books);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const sortByDate = async function (req, res, next) {
  try {
    const books = await bookModel.find({}).sort({ publishedDate: -1 }).exec();

    if (books) {
      res.status(200).json(books);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const addNewBook = async (req, res, next) => {
  const {
    title,
    thumbnailUrl,
    authors,
    publishedDate,
    genre,
    publisher,
    shortDescription,
    language,
    numIntakes,
    numAvailable,
  } = req.body;
  try {
    const newBook = new bookModel({
      _id: mongoose.Types.ObjectId(),
      title: title,
      thumbnailUrl: thumbnailUrl,
      authors: authors,
      publishedDate: publishedDate,
      shortDescription: shortDescription,
      genre: genre,
      publisher: publisher,
      language: language,
      numIntakes: numIntakes,
      numAvailable: numAvailable,
    });

    await newBook.save();
    res.status(201).json(newBook);
  } catch (error) {
    next(error);
  }
};

const deleteBook = async (req, res, next) => {
  const id = req.query.id;
  try {
    if (!id) {
      const error = new Error("Missing information!");
      error.status = 400;
      throw error;
    }
    const book = await bookModel.find({ _id: id }).exec();
    const bookDeleted = await bookModel.deleteOne({ _id: id }).exec();

    if (bookDeleted.deletedCount == 1) {
      res.status(200).json(book);
    } else {
      res.status(404).json();
    }
  } catch (error) {
    next(error);
  }
};

const reserveBook = async (req, res, next) => {
  const { id, username } = req.body;

  try {
    if (!id || !username) {
      const error = new Error("Missing information!");
      error.status = 400;
      throw error;
    } else {
      const book = await bookModel.findOne({ _id: id }).exec();
      const user = await userModel.findOne({ username: username }).exec();
      if (book == null || user == null) {
        res.status(404).json();
      } else {
        let available = book.numAvailable;
        let numIntakes = book.numIntakes;

        if (available == 0) {
          const error = new Error("No more books!");
          error.status = 400;
          throw error;
        }

        let reservedBooks = user.reserved;

        reservedBooks.push(id);

        available--;
        numIntakes++;
        const successBook = await bookModel.updateOne(
          { _id: id },
          { $set: { numAvailable: available, numIntakes: numIntakes } }
        );
        const bookUpdate = await bookModel.findOne({ _id: id }).exec();

        const successUser = await userModel.updateOne(
          { username: username },
          { $set: { reserved: reservedBooks } }
        );
        const userUpdate = await userModel
          .findOne({ username: username })
          .exec();

        res.status(200).json(userUpdate);
      }
    }
  } catch (error) {
    next(error);
  }
};

const getReserved = async (req, res, next) => {
  const username = req.params.username;

  try {
    if (!username) {
      const error = new Error(
        "Requested username is missing from the database."
      );
      error.status = 404;
      throw error;
    }
    const us = await userModel.findOne({ username: username }).exec();
    if (us) {
      res.status(200).json(us.reserved);
    } else {
      res.status(404).json();
    }
  } catch (error) {
    next(error);
  }
};

const getReservedTitles = async (req, res, next) => {
  const username = req.params.username;

  try {
    if (!username) {
      const error = new Error(
        "Requested username is missing from the database."
      );
      error.status = 404;
      throw error;
    }
    const us = await userModel.findOne({ username: username }).exec();
    const bs = await bookModel.find().exec();
    var books = [];
    const fav = us.reserved;

    bs.forEach((elem) => {
      fav.forEach((id) => {
        if (String(elem._id) == String(id)) books.push(elem.title);
      });
    });

    if (books) {
      res.status(200).json(books);
    } else {
      res.status(404).json();
    }
  } catch (error) {
    next(error);
  }
};

const removeFromReserved = async (req, res, next) => {
  const { id, username } = req.body;
  try {
    if (!id || !username) {
      const error = new Error("Id or username not found!");
      error.status = 400;
      throw error;
    } else {
      const book = await bookModel.findOne({ _id: id }).exec();
      const user = await userModel.findOne({ username: username }).exec();

      if (!user || !book) {
        res.status(404).json();
      } else {
        let userReserved = user.reserved;
        const resFilter = userReserved.filter((idBook) => idBook != id);

        let available = book.numAvailable;
        available++;
        const bookUpdate = await bookModel.updateOne(
          { _id: id },
          { $set: { numAvailable: available } }
        );
        const pom = await userModel.updateOne(
          { username: username },
          { $set: { reserved: resFilter } }
        );
        const userUpdated = await userModel
          .findOne({ username: username })
          .exec();
        res.status(200).json(userUpdated);
      }
    }
  } catch (error) {
    next(error);
  }
};

const getComments = async (req, res, next) => {
  const id = req.body.id;
  try {
    const bookObject = await bookModel.findOne({ _id: id }).exec();

    if (bookObject) {
      res.status(200).json(bookObject.comments);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const addComment = async (req, res, next) => {
  const { id, username, txt, date } = req.body;
  try {
    if (!id || !username) {
      const error = new Error("Id or username not found!");
      error.status = 400;
      throw error;
    } else if (!txt) {
      const error = new Error("Text not found!");
      error.status = 400;
      throw error;
    } else {
      const book = await bookModel.findOne({ _id: id }).exec();
      const user = await userModel.findOne({ username: username }).exec();

      if (!user || !book) {
        res.status(404).json();
      } else {
        let niz = book.comments;
        niz.push([username, txt, date]);

        const bookUpdate = await bookModel.updateOne(
          { _id: id },
          { $set: { comments: niz } }
        );
        res.status(200).json(bookUpdate);
      }
    }
  } catch (error) {
    next(error);
  }
};

const updateBook = async (req, res, next) => {
  const { id, title, authors, genre, publisher, language } = req.body;
  try {
    let update;

    if (title) {
      update = await bookModel.updateOne(
        { _id: id },
        { $set: { title: title } }
      );
    }
    if (authors) {
      update = await bookModel.updateOne(
        { _id: id },
        { $set: { authors: authors } }
      );
    }
    if (genre) {
      update = await bookModel.updateOne(
        { _id: id },
        { $set: { genre: genre } }
      );
    }
    if (publisher) {
      update = await bookModel.updateOne(
        { _id: id },
        { $set: { publisher: publisher } }
      );
    }
    if (language) {
      update = await bookModel.updateOne(
        { _id: id },
        { $set: { language: language } }
      );
    }
    const bookUpdate = await bookModel.find({ _id: id });

    res.status(201).json(bookUpdate);
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getBooks,
  getBookById,
  getBookByTitle,
  getBookByAuthor,
  getBooksByGenre,
  getBooksByLanguage,
  addNewBook,
  deleteBook,
  reserveBook,
  getReserved,
  removeFromReserved,
  addComment,
  getComments,
  updateBook,
  getReservedTitles,
  sortByTitle,
  sortByDate,
};
