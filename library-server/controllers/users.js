const usersExport = require("../models/users.js");
const validator = require("validator");
const booksExport = require("../models/book.js");

const books = booksExport.bookModel;
const users = usersExport.usersModel;
const User = usersExport;

const getAllUsers = async (req, res, next) => {
  try {
    const allUsers = await users.find({}).exec();
    res.status(200).json(allUsers);
  } catch (error) {
    next(error);
  }
};

const getUserByUsername = async (req, res, next) => {
  const username = req.params.username;
  try {
    if (!username) {
      const error = new Error(
        "Requested username is missing from the database."
      );
      error.status = 400;
      throw error;
    }
    const userObject = await users.findOne({ username: username }).exec();

    if (userObject) {
      res.status(200).json(userObject);
    } else {
      res.status(400).json();
    }
  } catch (error) {
    next(error);
  }
};

const addNewUser = async (req, res, next) => {
  const { username, email, password, imgUrl } = req.body;
  try {
    if (!username || !email || !password || !validator.isEmail(email)) {
      const error = new Error("Missing or invalid information!");
      error.status = 400;
      throw error;
    }

    const prevUser = await users.findOne({ username: username }).exec();
    if (prevUser !== null) {
      throw err;
    }
    
      
    const jwt = await User.registerNewUserWithPicture(username, password, email, imgUrl);

    
    return res.status(201).json({
      token: jwt,
    });
  } catch (error) {
    next(error);
  }
};

const loginUser = async (req, res, next) => {
  const { username } = req.body;
  try {
    const jwt = await User.getUserJWTByUsername(username);
    return res.status(201).json({
      token: jwt,
    });
  } catch (err) {
    next(err);
  }
};

const deleteUser = async (req, res, next) => {
  const username = req.params.username;

  try {
    if (!username) {
      const error = new Error("Username does not exist!");
      error.status = 400;
      throw error;
    } else {
      const isDeleted = await users.deleteOne({ username: username }).exec();
      if (isDeleted.deletedCount == 1) {
        res.status(200).json({ success: true });
      } else {
        res.status(404).json();
      }
    }
  } catch (error) {
    next(error);
  }
};

const changeUserPassword = async (req, res) => {
  const { username, oldPassword, newPassword } = req.body;

  if (!username || !oldPassword || !newPassword) {
    res.status(400).json();
  } else {
    const isChanged = await User.changeUserPassword(
      username,
      oldPassword,
      newPassword
    );
    if (isChanged) {
      res.status(200).json();
    } else {
      res.status(404).json();
    }
  }
};

const addToFavorites = async (req, res, next) => {
  const { id, username } = req.body;
  try {
    if (!id || !username) {
      const error = new Error("Id or username not found!");
      error.status = 400;
      throw error;
    } else {
      const book = await books.findOne({ _id: id }).exec();
      const user = await users.findOne({ username: username }).exec();
      let fav = user.favorites;
      if (!user || !book) {
        res.status(404).json();
      } else {
        fav.push(id);
        const pom = await users.updateOne(
          { username: username },
          { $set: { favorites: fav } }
        );
        res.status(200).json(user);
      }
    }
  } catch (error) {
    next(error);
  }
};

const getFavorites = async (req, res, next) => {
  const username = req.params.username;

  try {
    if (!username) {
      const error = new Error(
        "Requested username is missing from the database."
      );
      error.status = 404;
      throw error;
    }
    const us = await users.findOne({ username: username }).exec();
    if (us) {
      res.status(200).json(us.favorites);
    } else {
      res.status(404).json();
    }
  } catch (error) {
    next(error);
  }
};

const removeFromFavorites = async (req, res, next) => {
  const { id, username } = req.body;
  try {
    if (!id || !username) {
      const error = new Error("Id or username not found!");
      error.status = 400;
      throw error;
    } else {
      const book = await books.findOne({ _id: id }).exec();
      const user = await users.findOne({ username: username }).exec();
      let fav = user.favorites;
      if (!user || !book) {
        res.status(404).json();
      } else {
        const re = fav.filter((ajdi) => ajdi != id);
        const pom = await users.updateOne(
          { username: username },
          { $set: { favorites: re } }
        );
        res.status(200).json(user);
      }
    }
  } catch (error) {
    next(error);
  }
};

module.exports = {
  getUserByUsername,
  getAllUsers,
  addNewUser,
  loginUser,
  changeUserPassword,
  deleteUser,
  addToFavorites,
  getFavorites,
  removeFromFavorites,
};
