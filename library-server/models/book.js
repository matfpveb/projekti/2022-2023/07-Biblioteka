const mongoose = require('mongoose')
const bookSchema = new mongoose.Schema({
    _id:  mongoose.Schema.Types.ObjectId,
    title:{
        type: String,
        required: true,
    },
    thumbnailUrl: String,
    authors:[
        {
        type: String,
        }
    ],
    publishedDate: {
        type: Date,
        default: Date.now
    },
    shortDescription: String,
    genre: [
        {
        type: String,
        }
    ],
    publisher:{
        type:String
    },
    language: String,
    numIntakes: {
        type: Number,
        default: 0
    },
    numAvailable: {
        type: Number,
        default: 1
    },
    comments: {
        type: [[String, String, String]],
        default: []
    }
}, {collection: "books"});

const bookModel = mongoose.model('books', bookSchema)

module.exports = {bookModel}
