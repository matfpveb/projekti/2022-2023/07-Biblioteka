const mongoose = require("mongoose");
const bcrypt = require("bcrypt");
const jwtUtil = require("../utils/jwt");

const SALT_ROUNDS = 10;

const usersSchema = new mongoose.Schema(
  {
    _id: mongoose.Schema.Types.ObjectId,
    username: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    imgUrl: {
      type: String,
      default: 'assets/blank-profile.png',
    },
    favorites: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "books",
        default: [],
      },
    ],
    reserved: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "books",
        default: [],
      },
    ],
    librarian: {
      type: Boolean,
      default: false,
    },
    hash: {
      type: mongoose.Schema.Types.String,
      required: true,
    },
    salt: {
      type: mongoose.Schema.Types.String,
      required: true,
    },
    imgUrl: {
      type: mongoose.Schema.Types.String,
      required: false,
    }
  
  },
  { collection: "users" }
);

usersSchema.methods.setPassword = async function (password) {
  this.salt = await bcrypt.genSalt(SALT_ROUNDS);
  this.hash = await bcrypt.hash(password, this.salt);
};

usersSchema.methods.isValidPassword = async function (password) {
  return await bcrypt.compare(password, this.hash);
};

const usersModel = mongoose.model("users", usersSchema);

/**
 * Dohvata jedan dokument-korisnika iz baze.
 * @param {string} username Korisnicko ime.
 * @returns {Promise<mongoose.Document>} Dokument koji predstavlja korisnika za dato korisnicko ime.
 */
async function getUserByUsername(username) {
  const user = await usersModel.findOne({ username }).exec();
  return user;
}

/**
 * Kreira JSON Web Token sa podacima o korisniku.
 * @param {string} username Korisnicko ime.
 * @returns {Promise<string>} JWT sa podacima o korisniku sa datim korisnickim imenom.
 */
async function getUserJWTByUsername(username) {
  const user = await getUserByUsername(username);
  if (!user) {
    throw new Error(`User with username ${username} does not exist!`);
  }
  return jwtUtil.generateJWT({
    _id: user.id,
    username: user.username,
    email: user.email,
    favorites: user.favorites,
    reserved: user.reserved,
    librarian: user.librarian,
    imgUrl: user.imgUrl
  });
}

/**
 * Pamti novog korisnika u bazi podataka.
 * @param {string} username Korisnicko ime.
 * @param {string} password Lozinka.
 * @param {string} email Adresa elektronske poste.
 * @returns {Promise<string>} JWT sa podacima o novom korisniku.
 */
async function registerNewUser(username, password, email) {
  const user = new usersModel();
  user._id = new mongoose.Types.ObjectId();
  user.username = username;
  await user.setPassword(password);
  user.email = email;
  user.librarian = false;
  // user.imgUrl = "library-server\assets\blank-profile.png"

  await user.save();
  return getUserJWTByUsername(username);
}

/**
 * Pamti novog korisnika u bazi podataka.
 * @param {string} username Korisnicko ime.
 * @param {string} password Lozinka.
 * @param {string} email Adresa elektronske poste.
 * @param {string} imgUrl Slika.
 * @returns {Promise<string>} JWT sa podacima o novom korisniku.
 */
 async function registerNewUserWithPicture(username, password, email, imgUrl) {
  const user = new usersModel();
  user._id = new mongoose.Types.ObjectId();
  user.username = username;
  await user.setPassword(password);
  user.email = email;
  user.imgUrl = imgUrl;
  user.librarian = false;

  await user.save();
  return getUserJWTByUsername(username);
}

async function changeUserPassword(username, oldPassword, newPassword) {
  const user = await usersModel.findOne({ username }).exec();
  const newHash = await bcrypt.hash(newPassword, 10);

  if (await bcrypt.compare(oldPassword, user.hash)) {
    user.hash = newHash;
    const update=await usersModel.updateOne({username:username}, {$set:{hash:newHash}})
    return true;

  }
  return false;
}

async function changeUserProfileImage(username,imgUrl) {
  const user = await getUserByUsername(username);
  user.imgUrl = imgUrl;
  await user.save();
}


module.exports = {
  usersModel,
  getUserJWTByUsername,
  registerNewUser,
  getUserByUsername,
  changeUserPassword,
  changeUserProfileImage,
  registerNewUserWithPicture
};
