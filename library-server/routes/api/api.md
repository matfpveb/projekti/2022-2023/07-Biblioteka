## API

### /books route

| Request | Endpoint            | Description                      | Request parameters                                                                  | Response parameters |
| ------- | ------------------- | -------------------------------- | ----------------------------------------------------------------------------------- | ------------------- | ------ |
| GET     | /                   | Get all books                    |                                                                                     | \[Book]             |
| GET     | /title              | Get books by title               | title                                                                               | [Book]              |
| GET     | /genre              | Get books by genre               | genre                                                                               | [Book]              |
| GET     | /language              | Get books by language               | language                                                                               | [Book]              |
| GET     | /:id                | Get book by id                   | id                                                                                  | Book                |
| GET     | /reserved/:username | Get user's reserved books        | username                                                                            |                     | [Book] |
| GET     | /reservedTitles/:username | Get user's reserved books titles        | username                                                                            |                     | [string] |
| GET     | /sortByTitle | Get books sorted by titles        | title                                                                            |                     | [Book] |
| GET     | /sortByDate | Get books sorted by published date        | title                                                                            |                     | [Book] |
| POST    | /author             | Get books by author              | author                                                                              | [Book]              |
| POST    | /                   | Add new book                     | title, thumbnailUrl, authors,shortDescription, genre,publisher,language, numIntakes | Book                |
| POST    | /comments           | Get comments of a book           | id                                                                                  | comments            |
| PUT     | /reserve            | User reserved book by id         | id, username                                                                        | User                |
| PUT     | /reserved/:username    | Get user's reserved books        | username                                                                        | User.reserved                |
| PUT     | /removeReserved     | Remove user's book from reserved | id, username                                                                        | User                |
| PUT     | /addComment         | Add comment to a book            | id, username, txt, date                                                             | Book                |
| PUT     | /updateBook         | Update book                      | id, title, authors, genre, publisher,language, rating                               | Book                |
| DELETE     | /         | Delete book                      | id                           | Book                |

### /users route

| Request | Endpoint            | Description                      | Request parameters                 | Response parameters          |
| ------- | ------------------- | -------------------------------- | ---------------------------------- | ---------------------------- | -------- | --- |
| GET     | /                   | Get all users                    |                                    | [User]                       |
| GET     | /:username          | Get user's favorited books       | username                           | favorites                    |
| GET     | /favorite/:username | Get user by username             | username                           | User                         |
| POST    | /signup             | Register user                    | username, email, password          | jwt token                    |
| PUT     | /                   | Change user's password           | username, oldPassword, newPassword |                              |
| PUT     | /addFavorite        | Add book to favorites            | id, username                       | User                         |
| PUT     | /removeFavorite     | Remove book from user's favorite | id, username                       | UseDelete user from database | username |     |
