const multer  = require('multer');
const express = require('express')
const booksController = require('../../controllers/books')
const storage = require('../../utils/storage')
const router = express.Router()

router.get('/sortByTitle', booksController.sortByTitle)
router.get('/sortByDate', booksController.sortByDate)
router.get('/', booksController.getBooks)
router.get('/title', booksController.getBookByTitle)
router.get('/reservedTitles/:username', booksController.getReservedTitles)
router.get('/genre', booksController.getBooksByGenre)
router.get('/reserved/:username', booksController.getReserved)
router.get('/language', booksController.getBooksByLanguage)
router.get('/:id', booksController.getBookById)

router.put('/updateBook', booksController.updateBook)
router.put('/reserve', booksController.reserveBook);
router.put('/removeReserved', booksController.removeFromReserved)
router.put('/addComment', booksController.addComment)

router.post('/comments', booksController.getComments)
router.post('/author', booksController.getBookByAuthor)
router.post('/', multer({ storage: storage.storage }).single("image"), booksController.addNewBook)

router.delete('/', booksController.deleteBook)

module.exports = router