const express = require('express');
const controller = require('../../controllers/users');
const authentication = require('../../utils/authentication');

const router = express.Router();

router.get('/', controller.getAllUsers);
router.get('/:username', controller.getUserByUsername);
router.get('/favorite/:username', controller.getFavorites);

router.post('/signup', controller.addNewUser);
router.post('/login', authentication.canAuthenticate, controller.loginUser);

router.put('/', controller.changeUserPassword);
router.put('/addFavorite', controller.addToFavorites);
router.put('/removeFavorite', controller.removeFromFavorites);

router.delete('/:username', controller.deleteUser);

module.exports = router;
